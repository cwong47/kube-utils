# kube-utils

Collections of utilities and tools designed to interact with kubernetes to simplify daily life! ;)

# Motivation

This scripts are a bit more sophisticated than what an alias or a function can do, plus I can share them at work!

# Setup

Symlink the scripts to either `$HOME/bin` or `/usr/local/bin`, or add this repo to your `$PATH`.

# Contributors

Calvin Wong

# License

The utilities in this repo are distributed under the
[Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0),
see LICENSE and NOTICE for more information.
