#!/usr/bin/env bash
#
#  Author      : Calvin Wong
#  Date        : 20191108
#  What        : List all the namespaces of given label.
#  Requirement : kubectl

set -euo pipefail

fn_usage() {
    local exit="${1:-1}"

    echo "Usage: ${0##*/} [-h | -n namespace] [label-name]" 1>&2
    echo "       -h                     Print help." 1>&2
    echo "       -n namespace           Namespace to search." 1>&2
    echo "       label-name             Label Name to search." 1>&2
    exit $exit
}

while getopts ":hn:" opt; do
    case "$opt" in
        h)
            fn_usage 0
            ;;
        n)
            shift
            namespace="--namespace ${OPTARG:-}"
            ;;
        \?)
            echo "Invalid option: -$OPTARG" 1>&2
            fn_usage
            ;;
    esac
done
shift "$((OPTIND-1))"

label="${1:-app}"
namespace="${namespace:---all-namespaces}"

kubectl get pods \
    $namespace \
    --output custom-columns="NAMESPACE:.metadata.namespace,LABEL=${1:-app}:.metadata.labels.${1:-app}" \
    | awk '$2 !~ /<none>/ {print $0}' \
    | sort \
    | uniq
